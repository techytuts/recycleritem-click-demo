package com.androidexample.recycleritemclickdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        CustomRecyclerAdapter customRecyclerAdapter = new CustomRecyclerAdapter(createContentItems(), new CustomRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ContentItem contentItem) {
                Toast.makeText(getApplicationContext(), "clicked on text : " + contentItem.getText(), Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(customRecyclerAdapter);

    }

    private List<ContentItem> createContentItems() {
        List<ContentItem> contentItems = new ArrayList<>();
        for(int i = 1; i<=10; i ++){
            ContentItem contentItem = new ContentItem("Sample Text "+i,"Button Text "+i);
            contentItems.add(contentItem);
        }
        return contentItems;
    }
}
