package com.androidexample.recycleritemclickdemo;

import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sony on 26-01-2016.
 */
public class ContentItem {

    private String text;
    private String btnText;

    public ContentItem(String text, String btnText){
        this.text = text;
        this.btnText = btnText;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }
}
