package com.androidexample.recycleritemclickdemo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class CustomRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface OnItemClickListener{
        void onItemClick(ContentItem contentItem);
    }

    private final OnItemClickListener listener;
    private final List<ContentItem> contentItems;

    public CustomRecyclerAdapter(List<ContentItem> items, OnItemClickListener listener){
        this.listener = listener;
        this.contentItems = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        return new CustomRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CustomRecyclerViewHolder recyclerViewHolder = (CustomRecyclerViewHolder) holder;
        recyclerViewHolder.bind(contentItems.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return contentItems.size();
    }
}
