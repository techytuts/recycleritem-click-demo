package com.androidexample.recycleritemclickdemo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sony on 26-01-2016.
 */
public class CustomRecyclerViewHolder extends RecyclerView.ViewHolder {

    private TextView textView;
    private Button button;

    public CustomRecyclerViewHolder(View itemView) {
        super(itemView);
        this.textView = (TextView) itemView.findViewById(R.id.textView);
        this.button = (Button) itemView.findViewById(R.id.btn);
    }

    public void bind(final ContentItem contentItem, final CustomRecyclerAdapter.OnItemClickListener onItemClickListener){
        textView.setText(contentItem.getText());
        button.setText(contentItem.getBtnText());

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClick(contentItem);
            }
        });
    }

}
